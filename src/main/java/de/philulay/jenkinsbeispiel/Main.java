package de.philulay.jenkinsbeispiel;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Fakultaet faku = new Fakultaet();
		Fibonacci fib = new Fibonacci();
		
		int[] zahlen1 = fib.rekursiv(12);
		int[] zahlen2 = fib.iterativ(12);
		
		System.out.println("Fakultät von 7 iterativ: "+faku.iterativ(7));
		System.out.println("Fakultät von 9 rekursiv: "+faku.rekursiv(9));
		System.out.println("Fibonaccizahlen rekursiv: ");
		
		for(int i=0;i<10;i++){
			System.out.println("      "+i+".: "+zahlen1[i]);
		}
		System.out.println("Fibonaccizahlen iterativ: ");
		
		for(int i=0;i<10;i++){
			System.out.println("      "+i+".: "+zahlen2[i]);
		}
	}

}
